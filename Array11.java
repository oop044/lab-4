import java.util.Scanner;

public class Array11 {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int first,second;
        int arr[] = {3,5,2,1,4};
        while(true){
            for(int i = 0 ; i < arr.length ; i++){
                System.out.print(arr[i]+ " ");
            }
            System.out.println();

            System.out.print("Please input index: ");
            first = kb.nextInt();
            second = kb.nextInt();

            // Swap
            int temp = arr[first] ;
            arr[first] = arr[second];
            arr[second] = temp;

            boolean isFinish = true;
            for(int i = 1; i < arr.length-1; i++){
                if(arr[i]>arr[i+1]){
                    isFinish = false ;
                }
            }
            if(isFinish){
                System.out.println("You win!!!");
                break;
            }
        }
        
    }
}
