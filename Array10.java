import java.util.Scanner;

public class Array10 {
    public static void main(String[] args) {
        int size = 0; 
        int arr[];
        int uniarr[];
        int unisize = 0;
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input number of element: ");
        size = kb.nextInt();
        arr = new int[size];
        uniarr = new int[size];
        for(int i = 0; i < arr.length; i++){
            System.out.print("Element " + i +": ");
            arr[i] = kb.nextInt();
            int index = -1;
            for(int j = 0; j < unisize; j++){
                if(uniarr[j] == arr[i]){
                    index = j;
                }
            }
            if(index < 0){
                uniarr[unisize]= arr[i];
                unisize++;
            }
        }
        System.out.print("All number: ");
        for(int i = 0; i < unisize;i++){
            System.out.print(uniarr[i]+" ");
        }
    }
}
