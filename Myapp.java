import java.util.Scanner;

public class Myapp {
    public static void main(String[] args) {
        int choice = 0;
        while(true){
        printwelcome();
        printMenu();
        choice = choice();
        switch (choice) {
            case 1:
            HelloWorld();
                break;
            case 2:
            inputnumber();
                break;
            case 3:
            exitProgram();
                break;
        }
        }
        
    }

    private static void inputnumber() {
        int first;
        int second;
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input first number: ");
        first = kb.nextInt();
        System.out.print("Please input second number: ");
        second = kb.nextInt();
        int Result = first+second;
        System.out.println("Result = " + Result);
    }

    private static void HelloWorld() {
        int time;
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input time: ");
        time = kb.nextInt();
        for(int i = 0; i<time; i++){
            System.out.println("Hello World!!!");
        }
    }

    private static void exitProgram() {
        System.out.println("Bye!!");
        System.exit(0);
    }

    private static int choice() {
        int choice;
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.print("Please input your choice(1-3): ");
            choice = kb.nextInt();
            if (choice >= 1 && choice <= 3) {
                return choice;
            }
            System.out.println("Error: Please input between 1-3");
        }

    }

    private static void printMenu() {
        System.out.println("--Menu--");
        System.out.println("1. Print Hello World N times");
        System.out.println("2. Add 2 number");
        System.out.println("3. Exit");
    }

    private static void printwelcome() {
        System.out.print("Welcome to my app!!!");
    }
}
